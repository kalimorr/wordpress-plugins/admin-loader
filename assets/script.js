var AdminLoader = {};
(function ($) {

    /* Plugin Handler */
    AdminLoader = {

        /* Define the usefull classes */
        loaded : 'pageloaded',
        loading: 'loadpage',

        /**
         * Initialization method
         */
        init: function () {
            var self = this;
            // Cancel loader
            $(document).on('click', '.js-removeLoader', function (e) {
                $('body').removeClass(self.loading).addClass(self.loaded);
            });
        }
    };

    /* Admin page ready */
    $(document).ready(function () {
        AdminLoader.init();
    });

    /* Start of loading */
    window.addEventListener("beforeunload", function () {
        $('body').addClass(AdminLoader.loading).removeClass(AdminLoader.loaded);
    });

    /* End of loading */
    window.addEventListener("load", function () {
        $('body').removeClass(AdminLoader.loading).addClass(AdminLoader.loaded);
    }, false);

})(jQuery);